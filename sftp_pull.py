#%%
import pysftp
import os
import glob
import pandas as pd

#%%
# Connect SFTP
host = 'sftp.shiftdigital.com'
port = 22
username = 'mazda-drsales'
password= '8%Y?wHM%'
try:
  conn = pysftp.Connection(host=host,port=port,username=username, password=password)
  print("connection established successfully")
except:
  print('failed to establish connection to targeted server')

# %%
# Compare SFTP to Local files
local_file_list = glob.glob('data/DR*')
local_file_list = [x.replace('data/','') for x in local_file_list]

sftp_file_list = conn.listdir()

sftp_files_to_download =  list(set(sftp_file_list) - set(sftp_file_list))

#%%
# Download files that are not in local
if not sftp_files_to_download:
    print('no files to download')
else:
    for lead_file in sftp_file_list:
        conn.get(lead_file, f'data/{lead_file}')
# %%
# read and compile local files
# %%
# setting the path for joining multiple files
local_files_to_merge = os.path.join("data/", "DR Sales*")

# list of merged files returned
local_files_to_merge = glob.glob(local_files_to_merge)
# %%
# joining files with concat and read_csv
combined_files_df = pd.concat(map(pd.read_csv, local_files_to_merge), ignore_index=True)

# %%
combined_files_df.to_csv('data/shift_leads_matchback.csv',index=False)
# %%
