import pandas as pd
import os
import subprocess
import datetime
from dateutil.relativedelta import relativedelta

from sqlalchemy import create_engine

import mazdap as mz
import credentials
conn = mz.ObieeConnector(uid=credentials.uid, pwd=credentials.pwd)

os.environ["NLS_LANG"] = ".AL32UTF8"

engine = create_engine(
    "oracle+cx_oracle://RPR_STG:rpSgdEv12@x4ebid10.mnao.net:1521/EDWHDEV",
    max_identifier_length=128,
)

## START HERE ##

max_date_dt = (datetime.date.today() - relativedelta(months=1)).replace(day=1)
min_date_dt = max_date_dt - relativedelta(months=23)
max_date = max_date_dt.strftime("%Y-%m-%d")
min_date = min_date_dt.strftime("%Y-%m-%d")


def get_dim_tables():
    #### DLR/MKT ####
    dlr_master = conn.get_csv(
        "/shared/Operational Strategy/Governance and Process Innovation/WaiPhyo/Dim_Master/All Dealers"
    )
    dlr_master.to_csv("data/dim_dlr_master.csv", index=False)

    #### PARENT/DLR MAPPING ####
    parent_dlr = pd.read_sql("SELECT * FROM WP_PARENT_DEALER_MAPPING", engine)
    parent_dlr.to_csv("data/dim_parent_dlr.csv", index=False)

    #### DATE ####
    date_master = pd.DataFrame(
        {"cal_date_ym": pd.date_range(start=min_date, end=max_date, freq="MS")}
    )
    date_master.to_csv("data/dim_date_master.csv", index=False)

    print("Task: Dimension Tables - Completed.")


def get_cx():
    #### CX Sales ####
    cx_dump = []
    for i in pd.date_range(min_date, max_date, freq="MS"):
        dt = f"{i.year}{i.month:02}"
        cx_df = conn.get_excel(
            "/shared/Operational Strategy/Governance and Process Innovation/WaiPhyo/MDS/CX Sales Surveys",
            skiprows=2,
            filters=mz.obiee.define_filters(
                "comparison",
                "equal",
                '"Response Date"."Calendar Year Month Id"',
                {int(dt): "decimal"},
            ),
        )
        cx_df.columns = [
            "Dealer Code",
            "Calendar Date",
            "Survey ID",
            "Finance Manager OSAT",
            "Sales Likelihood to Recommend",
            "Sales Overall Satisfaction",
        ]
        cx_df["Calendar Date"] = cx_df["Calendar Date"].ffill()
        cx_dump.append(cx_df)
        print(f"Finished getting data for : {dt}")
    cx_sales = pd.concat(cx_dump)
    cx_sales.to_csv("data/fact_cx_sales.csv", index=False)

def get_retail():
    #### Pure Retail ####
    pure_retail_df = conn.get_csv(
        "/shared/Operational Strategy/Governance and Process Innovation/WaiPhyo/MDS/Mazda Pure CPO Retail"
    )
    pure_retail_df.to_csv("data/fact_pure_retail.csv", index=False)

def get_leads():
    #### Dealer Web Leads ####
    dlr_web_leads = pd.read_sql(
        f"SELECT * FROM WP_DS005_01_LEADS_ELMS_PROD WHERE VEHICLE_STATUS = 'new' \
        AND LEAD_TYPE IN ('DW Leads', 'DR Leads')  AND CALENDAR_DATE >= (DATE '{min_date}')",
        engine,
    )
    dlr_web_leads_matchback = pd.read_sql(
        f"SELECT * FROM WP_DS005_02_LEADS_MATCHBACK WHERE VEHICLE_STATUS = 'new' \
        AND LEAD_TYPE IN ('DW Matchback Leads', 'DR Matchback Leads') AND CALENDAR_DATE >= (DATE '{min_date}')",
        engine,
    )
    dlr_web_leads.to_csv("data/fact_dlr_web_leads.csv", index=False)
    dlr_web_leads_matchback.to_csv("data/fact_dlr_web_leads_matchback.csv", index=False)

def get_dlr_financials():
    #### Financials ####
    dlr_fin_df = conn.get_csv(
        "/shared/Operational Strategy/Governance and Process Innovation/WaiPhyo/MDS/MDS Fin"
    )
    dlr_fin_df.to_csv("data/fact_dlr_fin.csv", index=False)


def get_employee_tenure():
    job_codes = pd.read_csv("job_codes.csv")
    emp_df = []
    active_query = """SELECT emp.PRSN_ID_CD,
       emp.TRMNTN_DT,
       emp.HIRE_DT,
       emp.JOB_START_DT start_dt,
       emp.LOCTN_CD,
       jc.JOB_CD job_cd 
FROM edw_stg.BTC03020_DLR_EMPL emp 
LEFT JOIN edw_stg.BTC03050_JOB_CD jc ON jc.PRSN_ID_CD = emp.PRSN_ID_CD
WHERE (TRMNTN_DT >= TO_DATE(:report_month,'YYYYMMDD') OR TRMNTN_DT < TO_DATE('0001','YYYY'))
AND   HIRE_DT < TO_DATE(:report_month,'YYYYMMDD')
AND   JOB_START_DT < TO_DATE(:report_month,'YYYYMMDD')
""".strip()
    term_query = """
SELECT hist.PRSN_ID_CD, hist.job_cd, hist.START_DT , hist.END_DT FROM (
	SELECT PRSN_ID_CD, MAX(end_dt) end_dt 
	FROM edw_stg.BTC03030_JOB_CD_HSTRY 
    where end_dt >= to_date(:report_month, 'YYYYMMDD')
	GROUP BY PRSN_ID_CD 
) last_jb 
LEFT JOIN edw_stg.BTC03030_JOB_CD_HSTRY hist
ON last_jb.end_dt = hist.end_dt AND last_jb.prsn_id_cd = hist.PRSN_ID_CD 
"""
    for i in pd.date_range(min_date, max_date, freq="MS"):
        REPORT_MONTH = i.strftime("%Y%m%d")

        df = pd.read_sql(active_query, engine, params={"report_month": REPORT_MONTH})
        df["start_dt"] = pd.to_datetime(df["start_dt"], errors="coerce")
        df["trmntn_dt"] = pd.to_datetime(df["trmntn_dt"], errors="coerce")

        df_hist = pd.read_sql(term_query, engine, params={"report_month": REPORT_MONTH})
        df_hist["start_dt"] = pd.to_datetime(df_hist["start_dt"], errors="coerce")
        df_hist["end_dt"] = pd.to_datetime(df_hist["end_dt"], errors="coerce")

        df = df.merge(df_hist, how="left", on="prsn_id_cd", suffixes=["", "_h"])
        df.loc[df["job_cd"].isna(), "start_dt"] = df.loc[
            df["job_cd"].isna(), "start_dt_h"
        ]
        df.loc[df["job_cd"].isna(), "job_cd"] = df.loc[df["job_cd"].isna(), "job_cd_h"]
        df = df.drop(columns=["job_cd_h", "start_dt_h", "end_dt"])
        df["report_month"] = datetime.datetime.strptime(REPORT_MONTH, "%Y%m%d")
        df["tenure"] = (df["report_month"] - df["hire_dt"]).dt.days / 365

        df["job_tenure"] = (df["report_month"] - df["start_dt"]).dt.days / 365
        df = df.merge(job_codes, on="job_cd", how="inner")
        emp_df.append(df)
    return pd.concat(emp_df).to_csv("data/fact_emp_cnt_tenure.csv", index=False)

#### MAIN ####
# get_dim_tables()
# get_cx()
# get_retail()
get_leads()
# get_dlr_financials()
# get_employee_tenure()
#### MAIN ####

#### SYNC ONEDRIVE ####
# sync_cmd = "rclone sync -v data sharepoint:wphyo/mds_reporting --ignore-size --ignore-checksum --update"
sync_cmd = "rclone sync -v data onedrive_operational_strategy:wphyo/mds_reporting --ignore-size --ignore-checksum --update"

with open("logging.txt", "w") as f:
    process = subprocess.Popen(sync_cmd, shell=True, stderr=f, universal_newlines=True)
    while True:
        return_code = process.poll()
        if return_code is not None:
            break
#### SYNC ONEDRIVE ####

conn.logoff()

